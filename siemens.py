# -*- coding: utf-8 -*-
"""
Created on Wed Aug 25 20:36:26 2021
Python version of Siemens fGSLCalcPRS.h
@author: aaron


% All parameters From seimens protocol (text secion found in dicom header)
% if the field does not exist, then it = 0
%PosSCT:  sSliceArray.asSlice[0].sPosition.dSag
%         sSliceArray.asSlice[0].sPosition.dCor
%         sSliceArray.asSlice[0].sPosition.dTra
%NormSCT: sSliceArray.asSlice[0].sNormal.dSag
%         sSliceArray.asSlice[0].sNormal.dCor
%         sSliceArray.asSlice[0].sNormal.dTra
% This code is taken from the Siemens file fGSLCalcPRS.cpp
"""

import numpy as np

# Global constants
SAG = 0
COR = 1
TRA = 2 # index for 

#This is a c function, ported to python, it could be done much simpler in python, but to ensure exact function, the structure is kept
#function returns [phase, read, orienation] = 
def fGSLCalcPRS(NormSCT, inPlaneRoation):

  # First Calculate the orientation
  dAbsSagComp     = abs(NormSCT[SAG])
  dAbsCorComp     = abs(NormSCT[COR])
  dAbsTraComp     = abs(NormSCT[TRA])
  bAlmEqualSagCor = abs(NormSCT[SAG] - NormSCT[COR]) <= 1.e-6
  bAlmEqualSagTra = abs(NormSCT[SAG] - NormSCT[TRA]) <= 1.e-6 #%fGSLAlmEqual(dAbsSagComp, dAbsTraComp);
  bAlmEqualCorTra = abs(NormSCT[COR] - NormSCT[TRA]) <= 1.e-6 #%fGSLAlmEqual(dAbsCorComp, dAbsTraComp);

  orienation = -1
  #---------------------------------------------------------------------------
  #Check all values to determine the slice orientation (sag, cor, tra)
  #---------------------------------------------------------------------------
  if ((bAlmEqualSagCor              and  bAlmEqualSagTra)             or \
      (bAlmEqualSagCor              and  (dAbsSagComp < dAbsTraComp)) or \
      (bAlmEqualSagTra              and  (dAbsSagComp > dAbsCorComp)) or \
      (bAlmEqualCorTra              and  (dAbsCorComp > dAbsSagComp)) or \
      ((dAbsSagComp > dAbsCorComp)  and  (dAbsSagComp < dAbsTraComp)) or \
      ((dAbsSagComp < dAbsCorComp)  and  (dAbsCorComp < dAbsTraComp)) or \
      ((dAbsSagComp < dAbsTraComp)  and  (dAbsTraComp > dAbsCorComp)) or \
      ((dAbsCorComp < dAbsTraComp)  and  (dAbsTraComp > dAbsSagComp))):
  
    #-------------------------------------------------------------------------
    # Mainly transverse\
    #-------------------------------------------------------------------------
    orienation = TRA  
  else:
      if ((bAlmEqualSagCor              and  (dAbsSagComp > dAbsTraComp)) or \
           (bAlmEqualSagTra              and  (dAbsSagComp < dAbsCorComp)) or \
           ((dAbsSagComp < dAbsCorComp)  and  (dAbsCorComp > dAbsTraComp)) or \
           ((dAbsSagComp > dAbsTraComp)  and  (dAbsSagComp < dAbsCorComp)) or \
           ((dAbsSagComp < dAbsTraComp)  and  (dAbsTraComp < dAbsCorComp))):
  
           orienation = COR
      else:
          if ((bAlmEqualCorTra              and  (dAbsCorComp < dAbsSagComp)) or \
           ((dAbsSagComp > dAbsCorComp)  and  (dAbsSagComp > dAbsTraComp)) or \
           ((dAbsCorComp > dAbsTraComp)  and  (dAbsCorComp < dAbsSagComp)) or \
           ((dAbsCorComp < dAbsTraComp)  and  (dAbsTraComp < dAbsSagComp))):
            #-------------------------------------------------------------------------
            # Mainly sagittal\
            #-------------------------------------------------------------------------
            orienation = SAG
          else:
              #-------------------------------------------------------------------------
              # Invalid slice orientation\
              #-------------------------------------------------------------------------
              print('\n fGSLCalcPRS Error Slice Orientation invalid\n');

  phase = [0,0,0]
  read  = [0,0,0]
  
  if (orienation == TRA):    
	    phase[SAG] = 0;
	    phase[COR] = NormSCT[TRA] * np.sqrt (1. / (NormSCT[COR] * NormSCT[COR] + NormSCT[TRA] * NormSCT[TRA])) # %dGs[2] * sqrt (1. / (dGs[1] * dGs[1] + dGs[2] * dGs[2]));
	    phase[TRA] = -NormSCT[COR] * np.sqrt (1. / (NormSCT[COR]**2 + NormSCT[TRA]**2));                         #%-dGs[1] * sqrt (1. / (dGs[1] * dGs[1] + dGs[2] * dGs[2]));    
  elif (orienation == COR):
	    phase[SAG] = NormSCT[COR] * np.sqrt (1. / (NormSCT[SAG]**2 + NormSCT[COR]**2)) ;                        #%dGs[1] * sqrt (1. / (dGs[0] * dGs[0] + dGs[1] * dGs[1]));
	    phase[COR] = -NormSCT[SAG] * np.sqrt (1. / (NormSCT[SAG]**2 + NormSCT[COR]**2));                        #%-dGs[0] * sqrt (1. / (dGs[0] * dGs[0] + dGs[1] * dGs[1]));
	    phase[TRA] = 0.; 
  elif (orienation == SAG):
	    phase[SAG] = -NormSCT[COR] * np.sqrt (1. / (NormSCT[SAG]**2 + NormSCT[COR]**2));                        #%-dGs[1] * sqrt (1. / (dGs[0] * dGs[0] + dGs[1] * dGs[1]));
	    phase[COR] = NormSCT[SAG] * np.sqrt (1. / (NormSCT[SAG]**2 + NormSCT[COR]**2));                         #%dGs[0] * sqrt (1. / (dGs[0] * dGs[0] + dGs[1] * dGs[1]));
	    phase[TRA] = 0.;


  #%*------------------------------------------------------------------------*/
  #%*  Calculate GR = GS x GP                                                */
  #%*------------------------------------------------------------------------*/
  read[SAG] = NormSCT[COR] * phase[TRA] - NormSCT[TRA] * phase[COR];  #% dGs[1] * dGp[2] - dGs[2] * dGp[1];
  read[COR] = NormSCT[TRA] * phase[SAG] - NormSCT[SAG] * phase[TRA];  #% dGs[2] * dGp[0] - dGs[0] * dGp[2];
  read[TRA] = NormSCT[SAG] * phase[COR] - NormSCT[COR] * phase[SAG];  #% dGs[0] * dGp[1] - dGs[1] * dGp[0];


  if (inPlaneRoation != 0):
    dPhi = inPlaneRoation;
    #*----------------------------------------------------------------------*/
    # Rotate around the S axis                                             */
    #*----------------------------------------------------------------------*/
    phase[SAG] = np.cos (dPhi) * phase[SAG] - np.sin (dPhi) * read[SAG]; # dGp[0] = cos (dPhi) * dGp[0] - sin (dPhi) * dGr[0];
    phase[COR] = np.cos (dPhi) * phase[COR] - np.sin (dPhi) * read[COR]; # dGp[1] = cos (dPhi) * dGp[1] - sin (dPhi) * dGr[1];
    phase[TRA] = np.cos (dPhi) * phase[TRA] - np.sin (dPhi) * read[TRA]; # dGp[2] = cos (dPhi) * dGp[2] - sin (dPhi) * dGr[2];

    #*----------------------------------------------------------------------*/
    #* Calculate new GR = GS x GP                                           */
    #*----------------------------------------------------------------------*/
    read[SAG] = NormSCT[COR] * phase[TRA] - NormSCT[TRA] * phase[COR]; #% dGr[0] = dGs[1] * dGp[2] - dGs[2] * dGp[1];
    read[COR] = NormSCT[TRA] * phase[SAG] - NormSCT[SAG] * phase[TRA]; #% dGr[1] = dGs[2] * dGp[0] - dGs[0] * dGp[2];
    read[TRA] = NormSCT[SAG] * phase[COR] - NormSCT[COR] * phase[SAG]; #%	dGr[2] = dGs[0] * dGp[1] - dGs[1] * dGp[0];
  
  return phase,read,orienation



""" 
   Calculate the rotation matrix for the twix header
   return vectors for phase,read,normal,position
   phase,read and normal are unit vectors
   position is in mm
"""
def getOrientationVectors(hdr):
  pos = [0,0,0]
  pos[SAG] = hdr.Phoenix.get(('sSliceArray', 'asSlice', '0', 'sPosition', 'dSag'),0)
  pos[COR] = hdr.Phoenix.get(('sSliceArray', 'asSlice', '0', 'sPosition', 'dCor'),0)
  pos[TRA] = hdr.Phoenix.get(('sSliceArray', 'asSlice', '0', 'sPosition', 'dTra'),0)
  
  norm = [0,0,0]
  norm[SAG] = hdr.Phoenix.get(('sSliceArray', 'asSlice', '0', 'sNormal', 'dSag'),0)
  norm[COR] = hdr.Phoenix.get(('sSliceArray', 'asSlice', '0', 'sNormal', 'dCor'),0)
  norm[TRA] = hdr.Phoenix.get(('sSliceArray', 'asSlice', '0', 'sNormal', 'dTra'),0)
  
  dInPlaneRot = hdr.Phoenix.get(('sSliceArray', 'asSlice', '0', 'dInPlaneRot'),0)
  
  phase, read, orienation = fGSLCalcPRS(norm, dInPlaneRot)
  
  return phase,read,norm,pos

"""
   Calculate shift in Read and Phase direction of image
   hdr is hdr from Will Clarks python mapVBVD
"""
def getReadPhaseOffC(hdr):
    phase,read,norm,pos = getOrientationVectors(hdr)
    
    phase_off_c = np.matmul(pos,phase)
    read_off_c =  np.matmul(pos,read)
    return phase_off_c, read_off_c
  