# Mark Chiew
# mark.chiew@ndcn.ox.ac.uk
import numpy as np

fft  = lambda x, ax : np.fft.fftshift(np.fft.fftn(np.fft.ifftshift(x, axes=ax), axes=ax, norm='ortho'), axes=ax) 
ifft = lambda X, ax : np.fft.fftshift(np.fft.ifftn(np.fft.ifftshift(X, axes=ax), axes=ax, norm='ortho'), axes=ax) 


def espirit(x, dims=(128,128), kernel=(5,5), eig_thresh=0.02, mask_thresh=0.99):
    """
    Input:
        x: [Nc, Kx, Ky, Nz]
        dims: (Nx, Ny)

    Output:
        sens: [Nx, Ny, Nz, Nc]
    """

    if x.ndim < 4:
        x = x[:,:,:,np.newaxis]
    
    # Get dimensions
    Nc = x.shape[0]
    Nz = x.shape[3]
    Kx = x.shape[1]-kernel[0]+1
    Ky = x.shape[2]-kernel[1]+1
    pad_x = [np.int(i) for i in (np.ceil((dims[0]-kernel[0])/2), np.floor((dims[0]-kernel[0])/2))]
    pad_y = [np.int(i) for i in (np.ceil((dims[1]-kernel[1])/2), np.floor((dims[1]-kernel[1])/2))]
    sens  = np.zeros((Nc, dims[0], dims[1], Nz), dtype='complex')
    m  = np.zeros((dims[0], dims[1], Nz), dtype='complex')

    # Loop over slices (Nz)
    for z in range(Nz):
        # Initialise Hankel matrix
        H = np.zeros((Nc, np.prod(kernel), Kx*Ky), dtype='complex')
        for i in range(Kx):
            for j in range(Ky):
                # Populate Hankel matrix
                H[:,:,i*Ky+j] = x[:,i:i+kernel[0],j:j+kernel[1],z].reshape((Nc,-1))

        # Only keep svd singular vectors corresponding to above threshold singular values
        U,S,_ = np.linalg.svd(H.reshape((-1,Kx*Ky)), full_matrices=False)
        U = U[:,S>S[0]*eig_thresh]   

        # Get zero-paded kernel images
        U = U.reshape((Nc,kernel[0],kernel[1],-1))
        U = np.pad(U, ((0,0), pad_x, pad_y, (0,0)))
        U = np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(U, axes=(1,2)), axes=(1,2)), axes=(1,2))
        U = U*np.prod(dims)/np.sqrt(np.prod(kernel))

        # Perform voxel-wise SVD on coil x component matrices, voxelwise, keeping 1st component
        for i in range(dims[0]):
            for j in range(dims[1]):
                W,S,_ = np.linalg.svd(U[:,i,j,:], full_matrices=False)
                sens[:,i,j,z] = W[:,0]
                m[i,j,z] = S[0]
     # rotate phase relative to first channel and return
    return np.squeeze(sens*np.exp(-1j*np.angle(sens[[0],:,:,:]))*(np.abs(m)>mask_thresh))
    
    
 
def espirit_img(ximg, dims=(128,128), kernel=(5,5), eig_thresh=0.02, mask_thresh=0.99):
    """
    return sensity maps with images as input
    Input:
        ximg: [Nc, x, y, z]
        dims: (Nx, Ny)

    Output:
        sens: [Nx, Ny, Nz, Nc]
    """
    return espirit(fft(ximg,(1,2,3)),dims,kernel,eig_thresh,mask_thresh)