# mrsi_recon

Reconstruction code for MRSI
including Uzay Emir's COMCEPT at 7T
output nifti file compatable with FSL MRS

# concept.py Requires:
a modified pymapvbvd
download this from 
https://github.com/ironic-ath/pymapvbvd
install from its directory
pip install .

finufft: the flatron institues nufft
pip install finufft


Example using default settings 
import concept
concept.recon('my_twix.dat') #a nifit file will be saved as 'my_twix.nii'


# if alpha was used in the aquisition:
concept.recon('my_twix.dat',alpha=1.71,was_alpha_used_in_measurment=True)

# to reconstruct a different matrix size to the number of ringsx2
concept.recon('my_twix.dat',ImgRescale=1.5) # the matrix will be 1.5 times bigger (eg 32 becomes 48)

# specifcy the name of the output nifti file
concept.recon('my_twix.dat', fname_out='my_niftifile.nii')

