# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 14:11:07 2021
Reconstruct Uzay emers CONCEPT MRSI pulse sequecne

version for VB17 magnetom 7T

@author: aaron
"""

# Will Clark's mapVBVD version
import mapvbvd
import datetime, json, nibabel as nib
import numpy as np
import finufft  #flatron insitute nufft



#local  
import siemens, espirit

class Recon:
  def __init__(self,fname,alpha=1.,Nppr=64,ImgRescale=1,was_alpha_used_in_measurment=False,is_density_weighted=True,filter_kspace=False):
    """
      initiate paramters for reconstruction of concept
      fname : path to twix file
      optional:
        alpha=1, scale the FOV 
                 set larger than 1 will reconstruct a larger FOV, the Image matrix  
                 remains the same (i.e. lower resolution)
        was_alpha_used_in_measurment=False, set to True if sequence .ini file 
                 applied alpha to the chosen FOV (ensure correct FOV size)
        Nppr=64, default is 64, number RO points pere ring
        ImgRescale=1 rescale the image matrix (i.e. resolution)
                 set to 2 to double reconstructed matrix size, maintaining the FOV
        is_density_weighted (True), this is unused, and instead auto detected
        filter_kspace (False), reduces ringing, useful for unwighted scans
    """
    self.kdata = None
    self.img = None
    self.hdr = None
    self.dataArranged = False    
    self.fname_raw = fname
    
    self.Nppr = Nppr # allways 64, no way to detect otherwise, so initalise here
    self.ImgRescale = ImgRescale
    self.alpha = alpha
    self.Noffset = 8 # number of datapoints to offset RO by
    self.was_alpha_used_in_measurment = was_alpha_used_in_measurment
    self.Nti = 2 # assume two temporal interleaves
    self.is_dw = is_density_weighted # assume density weighting, not this is unused and instead auto detected
    self.filter_kspace = filter_kspace
    self.sens = None
    
  """ 
    Read in raw data and header from twix file
  """
  def getKdata(self):
    twixObj = mapvbvd.mapVBVD(self.fname_raw, removeOS=False)
    #fix missing ave loop counter for VB17
    lin = twixObj.image.Lin  #ring counter
    rep = twixObj.image.Rep # temporal interleave
    ave = twixObj.image.Ave
    
    # if averages loop unused, autodetect if this is density weighted or repetitions that should be averaged
    if(max(ave)<1):
        # recreate spatial interleaves - si as averages
        # spatial interleaves are not reported in VB17, in VE they may be in average dim
        si = np.cumsum(np.diff(lin,prepend=0)<0)
        NRep = twixObj.image.NRep
        si = si - rep*(max(si)+1)/NRep  # reset si counter for new TI
        if(max(si)>0):
           twixObj.image.fixLoopCounter('Ave',si) 
           self.is_dw = True
        else   :
           self.is_dw = False
           #assume all repetitions are temporal interleaves or averages
           twixObj.image.fixLoopCounter('Ave',np.floor(rep/self.Nti ))
           twixObj.image.fixLoopCounter('Rep',np.mod(rep,self.Nti ))
       
    
    self.kdata = twixObj.image['']
    self.hdr = twixObj.hdr
    self.kdims = twixObj.image.dataDims
    self.dataArranged = False 
    
    #keep a copy for debugging
    #self.kdata1 = self.kdata.copy()
    
  """ 
    Data coming in as: [0,COL x time/Col][1,Cha][2,ring/Lin][1][1][5,SI/Ave][1][7,Adc#/Eco][8,TI]
    Reorder the data to match:
    in matlab we had: [col=64][nSI/Ave=4][nRing/Lin=16][TI x time=1024][Cha=32]  
    python slises data differntly so we want:
    data to be [Ntime*Nti],[Ncha],[Nring*Nsi*Nppr]
    todo swap time and cha
    
    seperate_TIs=True will not mix the two tempoeral interleaves, keeping them seperate
  """
  def arrangeData(self,seperate_TIs=False):
    if not isinstance(self.kdata,np.ndarray):
      print('ConceptRecon.arrangeData: No data to arrange')
      return False
    
    if self.dataArranged:
      print('ConceptRecon.arrangeData: data already arranged')
      return False
    
    
    # if uniform weighted average the averages
    if not self.is_dw:
        self.kdata = self.kdata.mean(axis=5,keepdims=True)
   
    
    sz_kin = self.kdata.shape 

    
    Nadc = sz_kin[7]
    Nti = sz_kin[8]
    Ntime = int(Nadc*(sz_kin[0]-self.Noffset)/self.Nppr)
    Nsi = sz_kin[5]
    Nring = sz_kin[2]
    Ncha = sz_kin[1]
    
    Nseg = Nti # how much to segment up readout circle then to innterleave
    
    if(Nadc !=2):
        print('ConceptRecon.arrangeData: two echos expected in raw data '+str(sz_kin(2)) + ' found')
        return False
      
    
    # 1. Join the two ADC events
    # (16392, 32, 16, 1, 1, 4, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1)
    
    self.kdata = self.kdata.transpose((7,0,2,5,8,1,  3,4,6,9,10,11,12,13,14,15)).reshape((sz_kin[0]*Nadc,Nring,Nsi,Nti,Ncha))  #[col x time x adc][2,ring][1,SI][4,TI][5,Cha]
    # 2. strip first and last 8 points
    self.kdata = self.kdata[self.Noffset:-self.Noffset,:,:,:,:]
    
    #2. split col into  ppr/Nti, seg, time, and fix temporal ordering
   
    self.kdata = self.kdata.reshape((Ntime,Nseg,int(self.Nppr/Nseg),Nring,Nsi,Nti,Ncha))
    
    if Nti == 2:
      if not seperate_TIs:
        #print('ConceptRecon.arrangeData: mixing temporal interleaves Nti=2')
        tmp = self.kdata[:,1,:,:,:,0,:].copy()
        self.kdata[:,1,:,:,:,0,:] = self.kdata[:,0,:,:,:,1,:]
      else:
        print('ConceptRecon.arrangeData: skiping mixing temporal interleaves Nti=2')
        tmp = self.kdata[:,0,:,:,:,1,:].copy()
      self.kdata[:,0,:,:,:,1,:] = self.kdata[:,1,:,:,:,1,:]
      self.kdata[:,1,:,:,:,1,:] = tmp
    elif Nti > 2:
      print('ConceptRecon.arrangeData: cannot yet process more than two temporal interleaves')
    
    self.kdata = self.kdata.reshape((Ntime,self.Nppr,Nring,Nsi,Nti,Ncha)) #drop segments
    #2. re-order time,ti,cha,ring,si,col
    self.kdata = self.kdata.transpose((0,4,5,2,3,1)).reshape((Nti*Ntime,Ncha,self.Nppr*Nsi*Nring))
    
    self.dataArranged = True
    
    self.NRings = Nsi*Nring
    self.MeasuredMatrix = Nring*2  # record theh number of rings used to determin extend of k-space
    self.Nti = Nti
    self.Nsi = Nsi
    return True
  
  """
  replicate gen_k2_uzay.m, but no FOV/matrix or alpha scaling, output range -pi to pi
  """
  def getKTrajectory(self):
    
    N = 32768 # self.NRings*2;  % Uzay used the number of readout points 32768, .... we only needs to be >= 2 x total number rings
    ppr = self.Nppr
    kmax   = 0.5
    krange = np.linspace(0,kmax,N)
    
    
    
    if self.is_dw:
        # density weighting function
        y = (1+np.cos(np.linspace(0,np.pi,N)))*np.linspace(0,1,N)
        y = self.NRings*y/sum(y)    
        _,iA = np.unique(np.floor(np.cumsum(y)),return_index=True)
        r = krange[iA];
        dr = np.diff(r,append=kmax)
        r  = r+(2/3)*dr # nudge all rings out a bit
        density_weighting = np.ones((1,self.NRings))
        
    else:
        # uniform weighting
        # need to find out what the minimum radius is
        r = np.linspace(0,kmax,self.NRings)
        r = r +(1/2)*(kmax/self.NRings) #nudge out by half a unit (see doi 10.1002/nbm.3838), although there is a discrepancy in the plotted figure not obeying this
        density_weighting = np.linspace(0.75,self.NRings-0.25,self.NRings)  # compensate for change in density
        
    if self.filter_kspace:            
        # calculate density compensation
        #y = (1+np.cos(np.linspace(0,np.pi,self.NRings)))*np.linspace(0,1,self.NRings)
        y = (1+np.cos(-np.pi/4 + r*np.pi*2))*r  # r max is 0.5 therefore cos(pi - pi/4) shift means zero crossing is outside of our measured k-space
        y = y/max(y)
        i_max = np.argmax(y)
        y[0:i_max] = 1        
        density_weighting = y*density_weighting
        #
    
    kca = np.exp(1j*(np.linspace(0,ppr-1,ppr).reshape(ppr,1)*2*np.pi/ppr))* r.reshape(1,self.NRings)
    kca[:,::2] = kca[:,::2]*np.exp(1j*np.pi/ppr) # % shift odd rings by half a k point
    
    kca = kca.transpose((1,0)).reshape(np.prod(kca.shape))
    k = np.array([np.imag(kca),np.real(kca)]).transpose((1,0))
    
    return k,density_weighting
  
  """
  Generate an image with these dimentions
  [x][y][z][TI,TIME][Cha]
  
  """
  def calcImg(self):
    if not isinstance( self.kdata,np.ndarray):
      print('ConceptRecon.calcImg: No data to arrange')
      return False
    
    if not self.dataArranged:
      print('ConceptRecon.calcImg: data not arranged')
      return False
    
    self.ImgMatrix = int(np.round(self.MeasuredMatrix*self.ImgRescale))
    self.ImgRescale = self.ImgMatrix /self.MeasuredMatrix # alter scale to be compatable with rounding
    
    # CHA,TIME,X,Y,Z
    self.ImgSz = self.kdata.shape[0:-1] + (self.ImgMatrix ,self.ImgMatrix, 1 )  # 1 for 1 slice in Z
    
    #option1 sigpy library, was too slow when doing the spectral domain
    #coord = self.getKTrajectory()*self.ImgSz[-1]*self.alpha
    #self.img = fourier.nufft_adjoint(self.kdata, coord, oshape=self.ImgSz )#oversamp=self.Nsi
   
    coord,density_weighting = self.getKTrajectory()
    coord *= 2*np.pi*self.alpha/self.ImgRescale
    
    phase_oc,read_oc = siemens.getReadPhaseOffC(self.hdr)
    
    if self.was_alpha_used_in_measurment:
      self.FOV = self.hdr.Config.RoFOV 
    else:
      self.FOV = self.hdr.Config.RoFOV * self.alpha
      
    nVoxShift = np.array([read_oc,phase_oc])*self.ImgSz[-2]/self.FOV
    #for even reconstructed matrix size a half voxel shift is required
    if (self.ImgSz[-2]%2)==0:
      nVoxShift = nVoxShift+0.5
    phs = np.exp(1j*np.matmul(coord,nVoxShift))
    phs = (phs.reshape(self.NRings,self.Nppr)*(density_weighting.reshape((self.NRings,1)))).reshape(self.NRings*self.Nppr)
    

    # this is a 2d nufft
    self.img = finufft.nufft2d1( coord[:,0],coord[:,1],(self.kdata*phs).reshape((np.prod(self.ImgSz[0:2]),coord.shape[0])).astype('complex128'),(self.ImgSz[2:-1])) # exclude z dimention for recon....
    self.img = self.img.astype('complex64') #128 is overkill here, but seems only way finufft works
    self.img = self.img.reshape((self.ImgSz)) 
    
    # permute to make dimentions confirm to nifti with first three as spatial 
    # x,y,z,(3)time,(4)coil
    self.img = self.img.transpose([2,3,4,0,1])
    self.ImgSz = self.img.shape
    
    return True
    
  def loadSensMaps(self,fname_nii):
    """
    input:
        fname_nii, file name to load sensitivity maps from
    effect: 
        sens maps loaded to self.sens
    """
    sens = np.asanyarray(nib.load(fname_nii).dataobj)
    # test data consistent, X, Y and CH
    if sens.shape == (self.ImgSz[0:3]+(1,self.ImgSz[4])):
        self.sens = sens
    else:
        print('Cannot use sensitivies, dimentions do not agree, disgarding them')
        print(sens.shape)
 
    
  def combineCoils(self):
    """
        if no maps already loaded, calculate maps using first data point
        it is strongly recomended to use sensitvity maps calculated from a water spectra
    effect:
        sens maps recorded in self.sens
        images combined with self.sens
    """
    if isinstance(self.sens, type(None)):
        print('Calculating coil sensitivies with espirit')
        sens = espirit.espirit_img(self.img[:,:,:,0,:].transpose((3,0,1,2)),dims=self.ImgSz[0:2])
        self.sens = sens.reshape([self.ImgSz[4]]+list(self.ImgSz[0:3])+[1]).transpose((1,2,3,4,0))
  
    self.img = np.sum(self.img*np.conj(self.sens),axis=4)
    
  def saveSensMaps(self,fname_nii=''):
    """
    input:
        fname_nii, file name to save sensitivity maps to, if none given the raw data file appended with '_sens' will be used
    
    effect: 
        file saved
    """
    if len(fname_nii)==0:
        fname_nii = self.fname_raw.replace('.dat','_sens.nii.gz')
        
    if isinstance(self.sens, type(None)):
       print('No sens maps generated, skipping save')
    else: 
      self.saveIma2Nii(fname=fname_nii,print_headers=False,img_to_save=self.sens)
  
  def saveIma2Nii(self,fname='',print_headers=True,img_to_save=None):
    """
    input:
        fname, file name to save to nifti, if none given the rawdata filename with .nii.gz will be used
        print_headers: True to print info saved to nii
        img_to_save: used to save alternative images to an nii file such as sens maps
    effect: 
        file saved
    """
    if isinstance(img_to_save, type(None)):
        img_to_save = self.img.conj()
    
    p = self.hdr.Phoenix
    m = self.hdr.Meas
    
    spectrometer_frequency_hz = m.lFrequency * 1E-6
    nucleus_str = m.ResonantNucleus
    echo_time_s = p[('alTE','0')]*1e-6
    repetition_time_s = p[('alTR','0')]*1e-6
    dwelltime = p[('sRXSPEC', 'alDwellTime', '0')]*self.Nppr*1e-9/self.Nti # pixdim[4]
    
    if print_headers:
        print(f'EchoTime = {echo_time_s:0.3f} s')
        print(f'RepetitionTime = {repetition_time_s:0.3f} s\n')
    
    
    DeviceSerialNumber = str(int(m.DeviceSerialNumber))
    Manufacturer = m.Manufacturer
    ManufacturersModelName = m.ManufacturersModelName
    SoftwareVersions = m.SoftwareVersions
    
    if print_headers:
        print(f'DeviceSerialNumber = {DeviceSerialNumber}')
        print(f'Manufacturer = {Manufacturer}')
        print(f'ManufacturersModelName = {ManufacturersModelName}')
        print(f'SoftwareVersions = {SoftwareVersions}\n')
    
    PatientDoB = str(int(m.PatientBirthDay ))
    PatientName = m.tPatientName
    PatientPosition = m.tPatientPosition 
    if m.lPatientSex == 2:
      PatientSex = 'M'
    elif m.lPatientSex == 1:
      PatientSex = 'F'
    else:
      PatientSex = 'O'
      
    PatientWeight = m.flUsedPatientWeight

    if print_headers:
        print(f'PatientDoB = {PatientDoB}')
        print(f'PatientName = {PatientName}')
        print(f'PatientPosition = {PatientPosition}')
        print(f'PatientSex = {PatientSex}')
        print(f'PatientWeight = {PatientWeight}\n')
    
    
    conversion_method = 'Manual'
    conversion_time = datetime.datetime.now().isoformat(sep='T',timespec='milliseconds')
    original_file = [self.fname_raw]
    
    if print_headers:
        print(f'ConversionMethod = {conversion_method}')
        print(f'ConversionTime = {conversion_time}')
        print(f'OriginalFile = {original_file}')
    
    
    dim_dict = {'dim_5': 'DIM_COIL'}
    meta_dict = {**dim_dict,
            'SpectrometerFrequency':[spectrometer_frequency_hz,],
            'ResonantNucleus':[nucleus_str,],
            'EchoTime':echo_time_s,
            'RepetitionTime':repetition_time_s,             
            'DeviceSerialNumber':DeviceSerialNumber,
            'Manufacturer':Manufacturer,
            'ManufacturersModelName':ManufacturersModelName,
            'SoftwareVersions':SoftwareVersions,
            'PatientDoB':PatientDoB,
            'PatientName':PatientName,
            'PatientPosition':PatientPosition,
            'PatientSex':PatientSex,
            'PatientWeight':PatientWeight,
            'ConversionMethod':conversion_method,
            'ConversionTime':conversion_time,
            'OriginalFile':original_file}

    json_full = json.dumps(meta_dict)
    if print_headers:
        print(json_full)
    
    # Calculate the Orientation
    qform = self.getOrientation()
    newobj = nib.nifti2.Nifti2Image(img_to_save, qform)
    
    # Set q_form >0
    newobj.header.set_qform(qform)
    
    pixDim = newobj.header['pixdim']
    pixDim[4] = dwelltime
    #pixDim[3] = self.img.
    newobj.header['pixdim'] = pixDim
    
    # Set version information 
    newobj.header['intent_name'] = b'mrs_v0_3'
    
    # Write extension with ecode 44
    extension = nib.nifti1.Nifti1Extension(44, json_full.encode('UTF-8'))
    newobj.header.extensions.append(extension)
    
    if len(fname)==0:
      fname = self.fname_raw.replace('.dat','.nii.gz')
    # # From nii obj and write    
    nib.save(newobj,fname)


  def getOrientation(self):
    """
    output:
      qform: matrix for nifit header (4x4 affine transformation form pixel to patient space)
    
    notes:
    The convention is: +x = Right; +y = Anterior; +z = Superior. That is, moving in the positive x-direction (so that the x-coordinate increases) will be moving to the right, et
    Siemens coordinates are SAG (Left to Right), COR (Anterior to Posterior), TRA (Foot to Head)
    """
    phs,read,norm,pos = siemens.getOrientationVectors(self.hdr)
    qform = np.zeros([4,4])
    res = self.FOV/self.ImgSz[0]
    # must point to center of voxel (i.e. need 
    fov_shift_phs = np.array(phs)*(self.FOV-res)/2
    fov_shift_read =  np.array(read)*(self.FOV-res)/2
    slice_thickness = self.hdr.Meas.VoiThickness #sSpecPara.sVoI.dThickness
    qform[:-1,0] = np.array(read)*res
    qform[:-1,1] = np.array(phs)*res
    qform[:-1,2] = np.array(norm)*slice_thickness  #*self.hdr.  slice thickness hdr.Config
    qform[:-1,3] = np.array(pos)-fov_shift_phs-fov_shift_read
    qform[3,3] = 1
    
    #negate COR row to match +y being superior in nifti
    qform[1,:] = -qform[1,:]
    
    return qform

  def saveIma2mat(self,fname=''):
    """
    effect:
    Save self.img to matlab variable (.mat) using file name or raw data file.mat
    """
    from scipy import io
    if len(fname)==0:
      fname = self.fname_raw.replace('.dat','.mat')

    io.savemat(fname,{'img':self.img})

    
 
"""
 generic reconstruction scripts
"""

def recon(fname_in,fname_out='',**kwargs):
  """
  Assume all default parameters, perform all steps
  input: 
    fname_in: name of raw data file to use
  input (optional):
    fname_out:  name of nifti file to save
    alpha=1, scale the FOV 
             set larger than 1 will reconstruct a larger FOV, the Image matrix  
             remains the same (i.e. lower resolution)
    was_alpha_used_in_measurment=False, set to True if sequence .ini file 
             applied alpha to the chosen FOV (ensure correct FOV size)
    Nppr=64, default is 64, number RO points pere ring
    ImgRescale=1 rescale the image matrix (i.e. resolution)
             set to 2 to double reconstructed matrix size, maintaining the FOV
    filter_kspace (False), this reduces, useful for unwighted scans
  """
  my_recon = Recon(fname_in,**kwargs)
  my_recon.getKdata()
  my_recon.arrangeData()
  my_recon.calcImg()
  my_recon.saveIma2Nii(fname_out)
  return my_recon
  
def recon_uw(fname_in,fname_out='',**kwargs):
  my_recon = Recon(fname_in,is_density_weighted=False,filter_kspace=True,**kwargs)
  my_recon.getKdata()
  my_recon.arrangeData()
  my_recon.calcImg()
  my_recon.saveIma2Nii(fname_out)
  return my_recon


def reconAlpha171(fname_in,fname_out='',**kwargs):
  my_recon = Recon(fname_in,alpha=1.71,was_alpha_used_in_measurment=True,**kwargs)
  my_recon.getKdata()
  my_recon.arrangeData()
  my_recon.calcImg()
  my_recon.saveIma2Nii(fname_out)
  return my_recon
  
def recon_water(fname_in,fname_out='',sens_maps='',**kwargs):
    my_recon = Recon(fname_in,**kwargs)
    my_recon.getKdata()
    my_recon.arrangeData()
    my_recon.calcImg()
    my_recon.combineCoils()
    my_recon.saveSensMaps(sens_maps)
    my_recon.saveIma2Nii(fname_out)
    return my_recon

def recon_metab(fname_in,sens_maps,fname_out='',**kwargs):
    my_recon = Recon(fname_in,**kwargs)
    my_recon.getKdata()
    my_recon.arrangeData()
    my_recon.calcImg()
    my_recon.loadSensMaps(sens_maps)
    my_recon.combineCoils()
    
    my_recon.saveIma2Nii(fname_out)
    return my_recon